package com.sda.swing.example;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class WindowPanel {
    private JButton button;
    private JPanel mainPanel;
    private JLabel labelCenter;
    private int counter = 0;

   public WindowPanel() {

        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Mouse clicked");
                counter++;
                labelCenter.setText("Hello World!" + " - " + counter);
                super.mouseClicked(e);
            }
        });
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }
}
