package com.sda.swing.ex0;

import javax.swing.*;
import java.awt.*;

public class Window {
    private JFrame frame;
    private Formularz panel;

    public Window() {
        this.panel= new Formularz();
        this.frame = new JFrame();
        this.frame.setContentPane(this.panel.getMainPanel());
        this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.frame.setPreferredSize(new Dimension(640,480));
        this.frame.pack();
    }

    public void setVisible(boolean b) {
        this.frame.setVisible(b);
    }
}
