package com.sda.swing.ex0;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Formularz {
    private JLabel imie;
    private JLabel nazwisko;
    private JLabel rokUrodzenia;
    private JLabel plec;
    private JLabel stanCywilny;
    private JLabel numerButa;
    private JTextField textImie;
    private JTextField textNazwisko;
    private JTextField textRokUrodzenia;
    private JRadioButton radioMezczyzna;
    private JRadioButton radioKobieta;
    private JComboBox comboStanCywilny;
    private JSpinner spinnerNumerButa;
    private JButton buttonWyczysc;
    private JButton buttonZapisz;
    private JPanel MainPanel;

    public Formularz() {
        buttonWyczysc.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                resetTextButton(textImie,textNazwisko,textRokUrodzenia);
                resetRadioButton(radioMezczyzna, radioKobieta);
                resetComboBox(comboStanCywilny);
                resetSpinnerButton(spinnerNumerButa);
                super.mouseClicked(e);
            }
        });
        buttonZapisz.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    PrintWriter printWriter = new PrintWriter(new FileWriter("Zapisane.txt", true));
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Formularz{" + "imie= "+ textImie.getText() +", nazwisko= " + textNazwisko.getText() + ", rokUrodzenia= " + textRokUrodzenia.getText());
                    stringBuilder.append(", płeć= " + (radioKobieta.isSelected()? radioKobieta.getText():radioMezczyzna.getText()));
                    stringBuilder.append(", stan cywilny= " + comboStanCywilny.getSelectedItem().toString());
                    stringBuilder.append(", numer buta= "+ spinnerNumerButa.getValue().toString());
                    stringBuilder.append("}");
                    printWriter.println(stringBuilder.toString());
                    printWriter.close();
                } catch (IOException e1) {
                    System.out.println("File not found");
                }
                super.mouseClicked(e);
            }
        });
    }


    public void resetSpinnerButton(JSpinner... jSpinner){
        for (JSpinner spinner: jSpinner) {
            spinner.setValue(0);
        }
    }

    public void resetComboBox(JComboBox... jComboBox) {
        for (JComboBox combo: jComboBox) {
            combo.setSelectedIndex(0);
        }
    }

    public void resetRadioButton(JRadioButton... radioButton) {
        for (JRadioButton radio: radioButton) {
            radio.setSelected(false);
        }
    }

    public void resetTextButton(JTextField... textField) {
        for (JTextField text: textField) {
            text.setText(null);
        }
    }

    public JPanel getMainPanel() {
        return MainPanel;
    }
}


